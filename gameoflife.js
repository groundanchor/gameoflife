let rows = 24;
let cols = 24;
let playing = false;

//initialize
function initialize() {
    createTable();
    setUpControlButtons();
}

//create table
function createTable() {
    let gridContainer = document.getElementById("gridcontainer");
    if (!gridContainer) {
        //throw error
        console.error("Problem...no container available for table.");
    }
    let table = document.createElement("table");
    for (i = 0; i <= rows; i++) {
        var tr = document.createElement("tr");
        for (j = 0; j <= cols; j++) {
            var cell = document.createElement("td");
            cell.setAttribute("id", i + "_" + j);
            cell.setAttribute("class", "dead");
            cell.onclick = cellClickHandler;
            tr.appendChild(cell);
        }
        table.appendChild(tr);
    }
    //add table tp grid container
    gridContainer.appendChild(table);
}

function cellClickHandler() {
    let currentClass = this.getAttribute("class");
    if (currentClass.indexOf("live") > -1) {
        this.setAttribute("class", "dead");
    } else {
        this.setAttribute("class", "live");t
    }
}

function setUpControlButtons(){
    let elStartButton = document.getElementById("start");
    elStartButton.addEventListener('click', function(){startButtonHandler(playing)}, false);
    let elClearButton = document.getElementById("clear");
    elClearButton.addEventListener('click', clearButtonHandler(playing), false);
    
}

function startButtonHandler(currentStatus){
    
}

function clearButtonHandler(currentStatus){
    
}

//start everyting
window.onload = initialize;


